package entity;

import virtualworld.VirtualMap;

public class PlayerShip extends SimpleShip {
    
    private static int PRIMARY_BULLET_WIDTH = 8;
    private static int PRIMARY_BULLET_HEIGHT = 8;
    private static int WIZ_BULLET_WIDTH = 8;
    private static int WIZ_BULLET_HEIGHT = 8;
    private static int UP_ANGLE = 90;
    private static int WIZ_ANGLE = 45;
    private int tempx=0;
    private int tempy=0;
    private Weapons currentFire;
    
    public PlayerShip(int x, int y) {
        super(x, y);
        super.life=150;
        currentFire = Weapons.frontfire;
    }
    
    @Override
    public void update() {
    	synchronized (this) {
    		this.getBody().move(tempx, tempy);
		}
    }

    @Override
    public String getType() {
        return "Player";
    }

    @Override
    public Faction getFaction() {
        return Faction.ALLY;
    }

    @Override
    protected int getBulletWidth() {
    	if(currentFire == Weapons.frontfire) 
    	{
            return PRIMARY_BULLET_WIDTH;
    	}
    	else
    	{
    		return WIZ_BULLET_WIDTH;
    	}
    }

    @Override
    protected int getBulletHeight() {
    	if(currentFire == Weapons.frontfire) 
    	{
            return PRIMARY_BULLET_HEIGHT;
    	}
    	else
    	{
    		return WIZ_BULLET_HEIGHT;
    	}
    }
    
    
    
    public void fire() {
        if(currentFire == Weapons.frontfire)
        {
            shoot(UP_ANGLE);
        }
        else
        {
        	shoot(UP_ANGLE - WIZ_ANGLE);
        	shoot(UP_ANGLE + WIZ_ANGLE);
        }
    }
    
    private enum Weapons{
        frontfire,
        wiz;
    }

    @Override
    public void setMap(VirtualMap map) {
        // TODO Auto-generated method stub
    }

	@Override
	public int getScoreValue() {
		return 0;
	}
	
	public void move(int x, int y){
		synchronized (this) {
			this.tempx=x;
			this.tempy=y;
		}		
	}
}
